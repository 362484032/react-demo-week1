import React from 'react'
/* 
  1、原生JS中创建节点
  let 节点元素对象=document.createElement('标签名称')
  给节点添加元素内容:节点对象.innnerText=渲染文本
  给元素添加HTML内容：节点对象.innerHTML=HTML内容
  给元素添加属性:节点对象.setAttribute(属性名,属性值)
  2、原生JS中DOM操作
  追加元素到父元素:父元素.appendChild(子元素)
  3.React中创建节点：使用JSX的使用完成创建
    <h1>Hello React</h1>
  
  注意点:
  1) 返回的JSX在结构上必须要有一个根节点才行
  2) 建议使用小括号()将这个结构括起来
  3) 建议将根节点使用<></>做为根节点
*/
export default function App() {
  let template = (
    <>
        <h1>Hello React</h1>
        <h2>Hello Giles</h2>
    </>
  )
  return template
}
