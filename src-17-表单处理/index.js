/*
 index.js是React项目的入口文件 
 */
//导入react包，react是react框架的核心包
import React from 'react';
//导入reactdom,ReactDOM是React操作DOM的核心包
import ReactDOM from 'react-dom/client';
//导入App组件，App组件是整个项目的根组件
import App from './App';
//将App根节点渲染到ID的节点上
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App />
);