import React, { Component,createRef } from 'react'

export default class Right extends Component {
 constructor(){
   super();
   //this.nameInput=createRef()
   this.ageInput=createRef()
 }
 register=(e)=>{
    e.preventDefault()
    //let name=this.nameInput.current.value
    let age=this.ageInput.current.value
    //console.log('姓名:',name);
    console.log(this.nameInput);
    console.log('年龄',age);
 }
  render() {
    return (
      <div style={{height:'100vh',flex:'1',backgroundColor:'pink'}}>
          <h1>非受控组件</h1>
          <form onSubmit={this.register}>
            <div>
                <label>姓名:</label>
                <input type="text" placeholder='请输入用户名' ref={a=>this.nameInput=a}/>
            </div>
            <div>
              <label>年龄:</label>
              <input type="text" placeholder='请输入年龄' ref={this.ageInput}/>
            </div>
            <div>
              <input type="submit" value="提交" />
            </div>
        </form>
      </div>
    )
  }
}
