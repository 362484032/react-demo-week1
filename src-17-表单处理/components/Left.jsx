import React, { Component } from 'react'
/* 
  受控组件完成表单数据的获取步骤
  1.定义状态数据
  2.将类组件中的状态属性赋值给表单元素的value属性
  3.在input或者textarea或者select表单元素绑定onChange事件
*/
export default class Left extends Component {
  state={
    name:'',
    age:''
  }
  handleInputName=(e)=>{
    this.setState(()=>{
      return{
        name:e.target.value
      }
    })
  }
  render() {
    return (
      <div style={{height:'100vh',flex:'1',backgroundColor:'yellow'}}>
        <h1>受控组件</h1>
        <form>
            <div>
                <label>姓名:</label>
                <input type="text" placeholder='请输入用户名' value={this.state.name} onChange={this.handleInputName}/>
            </div>
            <div>
              <label>年龄:</label>
              <input type="text" placeholder='请输入年龄'  value={this.state.age} onChange={e=>{this.setState(()=>({age:e.target.value}))}}/>
            </div>
            <div>
              <input type="submit" value="提交" />
            </div>
        </form>
      </div>
    )
  }
}
