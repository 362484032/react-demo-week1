import React, { Component } from 'react'
/* 
  this.setState()
  1.this.setState()是一个异步操作
  2.可以调用多次this.setState()，但是会执行最后一次的setState()
  3.不管执行多次setState()但是render渲染方法只会执行一次
*/
export default class Counter extends Component {
  state={
    count:0
  }
  increment1=()=>{
    this.setState((state,props)=>{
      console.log('第1次调用setState方法');
      return{
        count:this.state.count+1
      }
    },()=>{
      console.log('更新之后的count值',this.state.count);
    })
    console.log('更新之前的coun值',this.state.count);
    // this.setState((state,props)=>{
    //   console.log('第2次调用setState方法');
    //   return{
    //     count:this.state.count+1
    //   }
    // })
  }
  //如下的方法不推荐使用
  increment=()=>{
    this.setState({
      count:this.state.count+1
    })
    // this.setState({
    //   count:this.state.count+2
    // })
    // this.setState({
    //   count:this.state.count+3
    // })
    console.log('count',this.state.count);
  }
  render() {
    console.log('----render-----');
    let {count}=this.state
    return (
      <div>
        <h1>计数器:{count}</h1>
        <button onClick={this.increment}>+</button>
        <button onClick={this.increment1}>+1</button>
      </div>
    )
  }
}
