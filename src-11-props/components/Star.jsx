import React, { Component } from 'react'
import './star.css'
export default class Star extends Component {
  render() {
    console.log(this.props);
    return (
      <div className='box'>
         <img src={this.props.headImg} alt="" />
         <div className='title'>{this.props.name}</div>
         {/* <img src={this.props.obj.headImg} alt="" />
         <div className='title'>{this.props.obj.name}</div> */}
      </div>
    )
  }
}
