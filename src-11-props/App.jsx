import React, { Component } from 'react'
import Star from './components/Star'
export default class App extends Component {
  state={
    s1:{
      headImg:'https://img1.baidu.com/it/u=636284837,3827576918&fm=253&fmt=auto&app=120&f=GIF?w=542&h=500',
      name:'刘亦菲'
    },
    s2:{
      headImg:'https://img1.baidu.com/it/u=1759334853,157694390&fm=253&fmt=auto&app=120&f=JPEG?w=400&h=382',
      name:'宋慧乔'
    },
    s3:{
      headImg:'https://img2.baidu.com/it/u=1853157349,2304908556&fm=253&fmt=auto&app=120&f=JPEG?w=544&h=544',
      name:'王心凌'
    }
  }
  render() {
    return (
      <div>
        {/* <Star headImg={this.state.s1.headImg} name={this.state.s1.name}></Star>*/}
        {/* <Star obj={this.state.s1}></Star> */}
        <Star {...this.state.s1}></Star>
        <Star {...this.state.s2}></Star>
        <Star {...this.state.s3}></Star>
        
      </div>
    )
  }
}
