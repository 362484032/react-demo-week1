import React from 'react'
import HelloWorld from './components/HelloWorld'
export default function App() {
  return (
    <div>
        <HelloWorld title="Hello Giles"></HelloWorld>
        <hr />
        <HelloWorld title="Hello Jerry"></HelloWorld>
        <hr />
        <HelloWorld></HelloWorld>
    </div>
  )
}
