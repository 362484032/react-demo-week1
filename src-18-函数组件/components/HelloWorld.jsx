/* 
    一、函数式组件创建的规则
    1.React使用普通函数或者箭头函数来进行函数式组件的创建
    2.函数名或者变量符合驼峰式命名法，建议首字母大写
    3.这个函数必须要有返回值
    4.返回值就是JSX
    5.为了避免分号陷阱，建议return后面使用小括号括起来
    6.JSX必须要有一个跟组件，可以使用<></>做为跟标签
    7.为了加入到组件系统中，必须要进行导出
    二、函数组件的事件处理
    由于函数组件没有this,调用事件处理函数无需使用this
    三、函数组件没有状态，如果在函数组件要使用状态，要等到16.8之后版本，因为16.8出现了一个Hook
    四、函数组件没有生命周期 
*/
import React from 'react'
import PropTypes from 'prop-types'

export default function HelloWorld(props) {
  const handleClick=()=>{
    console.log('调用事件处理函数');
    console.log(props);
  }
  return (
    <div>
        <h1>{props.title}</h1>
        <button onClick={handleClick}>点击我</button>
    </div>
  )
}
//函数名.defaultProps={}
HelloWorld.defaultProps={
    title:'Hello Woniuxy'
}
//函数名.propTypes={}
HelloWorld.propTypes={
    title:PropTypes.string.isRequired
}
