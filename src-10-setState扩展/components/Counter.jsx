import React, { Component } from 'react'
/* 
  call:可以改变this指向，并且调用函数，参数参数序列 语法:方法名.call(目标对象,参数1，参数2,参数n)
  apply：可以改变this指向，并且调用函数,参数是数组  语法：方法名.apply(目标对象,[参数1,参数2,参数n])
  bind: 新方法名=方法名.bind(目标对象)  调用:  新方法名(参数1，参数2，参数n)

  React事件绑定的this指向问题
  1.在类组件中普通函数存在this指向问题（开发中尽量不要用，不要涉及）
  怎么改变this问题
  位置1：可以在构造函数中改变，使用bind来改变 this.普通函数=this.普通函数名.bind(this)
  位置2: <button onClick={this.普通函数名.bind(this)}></button>
*/
export default class Counter extends Component {
  // constructor(){
  //   super()
  //   console.log('构造函数',this);
  //   //改变this指向
  //   this.increment=this.increment.bind(this)
  // }
  state={
      count:0
  }
  // increment=()=>{
  //   console.log(this);
  // }
  increment(){

    this.setState({
      count:this.state.count+1
    })
  }
  render() {
    let {count}=this.state
    const increment2=()=>{
      console.log(this);
    }
    return (
      <div> 
          <h1>计数器：{count}</h1>
          <button onClick={this.increment.bind(this)}>+</button>
          <button onClick={()=>{
            this.setState({
              count:this.state.count+1
            })
          }}>+</button>

          <button onClick={increment2}>+</button>
      </div>
    )
  }
}
