import React, { Component } from 'react'
/* 
    子组件向父组件传值的思路
    1.在子组件的的某一个控件上触发事件，完成向父组件的props的回调函数传值  this.props.回调函数(需要传递的值)
    2.在父组件引用子组件标签中<子组件 回调函数名={事件处理函数}>
    3.在事件处理函数接收数据，并赋值给当前组件的状态
*/
export default class Child extends Component {
  sonPostToFather=()=>{
    //在子组件中调用了父组件传递给它的函数，这个函数较callback
    this.props.callback('给我200块钱')
  }
  render() {
    return (
      <div>
          <h2>我是子组件</h2>
          <button onClick={this.sonPostToFather}>儿子向父亲发送数据</button>
      </div>
    )
  }
}
