import React, { Component } from 'react'
import Child from './Child'
export default class Parent extends Component {
  state={
      message:''
  }
  getMsgFromSon=(msg)=>{
      console.log('((((((((((((((((((('+msg);
      this.setState(()=>{
          return{
              message:msg
          }
      })
  }
  render() {
    return (
      <div>
          <h1>我是父组件</h1>
          <div style={{width:'200px',border:'1px dashed #000',padding:'10px',backgroundColor:'yellow'}}>{this.state.message}</div>
          <Child callback={this.getMsgFromSon}></Child>
      </div>
    )
  }
}
