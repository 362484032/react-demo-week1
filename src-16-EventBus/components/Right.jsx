import React, { Component } from 'react'
import MyEventListener from '../utils/EventListener'

export default class Right extends Component {
    state={
        msg:''
    }
    componentDidMount(){
        MyEventListener.addListener("dataChange",this.getBrotherMsg)
    }
    componentWillUnmount(){
        MyEventListener.removeListener("dataChange",this.getBrotherMsg)
    }
    getBrotherMsg=(msg)=>{
        this.setState(()=>{
            return{msg}
        })
    }
    render() {
        return (
            <div style={{ height: '100vh', flex: '1', backgroundColor: 'springgreen' }}>
                <h1>Right</h1>
                <div>
                    {this.state.msg}
                </div>
            </div>
        )
    }
}
