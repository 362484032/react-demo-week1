import React, { Component,createRef } from 'react'
/* 
    React基于事件总线方式进行兄弟组件直接传值的步骤
    1.下载events: yarn add events
    2.创建EventListener.js文件，具体内容如下所示
       import {EventEmitter} from 'events'
       export default new EventEmitter()
    3.编写发送方的代码
       MyEventListener.emit("dataChange",msg)
    4.编写接收方的代码
       componentDidMount(){
        MyEventListener.addListener("dataChange",this.getBrotherMsg)
        }
        componentWillUnmount(){
            MyEventListener.removeListener("dataChange",this.getBrotherMsg)
        }
*/

import MyEventListener from '../utils/EventListener'
export default class Left extends Component {
  constructor(){
    super()
    this.inputElement=createRef()
  }
  send=()=>{
    /* 
        EventEmitter实例中相关方法的说明
        emit(参数1，参数2)：用来发送信息到兄弟组件的，参数说明
        参数1：发送信息的标识,也就是事件名称
        参数2：发送信息的具体内容 
    */
    let msg=this.inputElement.current.value
    MyEventListener.emit("dataChange",msg)
  }
  render() {
    return (
      <div style={{height:'100vh',flex:'1',backgroundColor:'yellow'}}>
        <h1>Left</h1>
        <input type="text" placeholder='请输入' ref={this.inputElement}/>
        <button onClick={this.send}>发送</button>
      </div>
    )
  }
}
