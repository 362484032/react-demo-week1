import React, { Component } from 'react'
import Left from './components/Left'
import Right from './components/Right'
export default class App extends Component {
  render() {
    return (
      <div style={{display:'flex'}}>
         <Left></Left>
         <Right></Right>
      </div>
    )
  }
}
