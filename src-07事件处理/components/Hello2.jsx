import React, { Component } from 'react'

export default class Hello2 extends Component {
  //如果要将事件处理函数定义为普通函数，并放在类组件中，作为类组件的成员
  handleClick1(){
      console.log('我被按钮1给点击了');
  }
  hanleClick2=()=>{
      console.log('我被按钮2给点击了');
  }
  render() {
    return (
      <div>
          <button onClick={this.handleClick1}>按钮1</button>
          <button onClick={this.hanleClick2}>按钮2</button>
      </div>
    )
  }
}
