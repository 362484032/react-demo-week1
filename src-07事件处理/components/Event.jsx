import React, { Component } from 'react'
/* 
   参数对象
   第1种情况：如果在调用的时候不传递任何实参，形参就为event对象
   第2种情况：额外参数，是一一对应关系
   第3种情况：event和额外参数一起传递

   回顾
   1.原生js:一一对象
   2.微信小程序 data-参数
   3.vue传参数: $event
*/
export default class Event extends Component {
  handleClick1(e){
    console.log(e);
  }
  handleClick2(p1,p2,p3){
    console.log(p1,p2,p3);
  }
  handleClick3(e,p1,p2){
    console.log('第2步：从箭头函数中传递进来',e);
    console.log(p1,p2);
  }
  handleClick4(e){
    e.preventDefault();
    console.log('我爱你Giles');
  }
  render() {
    return (
      <div>
          <button onClick={this.handleClick1}>按钮1</button>
          <button onClick={()=>{this.handleClick2('Giles',38,'男')}}>按钮2</button>
          <button onClick={(e)=>{
              console.log('第1步骤：从按钮本身传递进来',e);
              this.handleClick3(e,'Giles',38)

          }}>按钮3</button>
          <a href="http://www.woniuxy.com" onClick={this.handleClick4}>进来看看</a>
      </div>
    )
  }
}
