import React, { Component } from 'react'
/* 
    1.回顾事件处理
    1)原生js的事件处理：
     绑定事件处理的方式
     1）HTML标签的方式<h1 onclick="事件处理"></h1>
     2) DOM0级事件绑定: element.on事件类型=事件处理函数
     3) DOM2级事件绑定：element.addEventListener('事件类型',回调函数)
    2) vue中事件绑定：v-on:事件类型 简写形式 @click=""
    3) 微信小程序绑定: bindtap="" catchtap=""
    2.react事件绑定的方式(类组件)
    第1种方式：onClick={()=>{}}
    第2种方式：onClick={this.事件名称} 备注：事件是定义在类组件内部，和render方法平级关系

*/
export default class Hello extends Component {
  render() {
    let handleClik1=()=>{console.log('你把我点击了！！！')}
    return (
      <div>
          <button onClick={()=>{console.log('你把我点击了！！！')}}>点击我</button> {/* react事件绑定方式1的第一种写法 */}
          <button onClick={handleClik1}>点击我</button>
      </div>
    )
  }
}
