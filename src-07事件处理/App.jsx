import React from 'react'
import Hello from './components/Hello'
import Hello2 from './components/Hello2'
import Event from './components/Event'
export default function App() {
  return (
    <div>
      <Hello></Hello>
      <hr />
      <Hello2></Hello2>
      <hr />
      <Event></Event>
    </div>
  )
}

