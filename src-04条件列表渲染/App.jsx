import React from 'react'
import './app.css'
export default function App() {
  let persons=[
    {
      id:'1001',
      name:'刘备',
      job:'皇帝',
      amount:1200,
      gender:1,
    },
    {
      id:'1002',
      name:'关羽',
      job:'大将',
      amount:200,
      gender:1
    },
    {
      id:'1003',
      name:'张飞',
      job:'大将',
      amount:21200,
      gender:1
     },
     {
      id:'1004',
      name:'貂蝉',
      job:'美女',
      amount:500,
      gender:0
     }
  ]
  const getLevel=amount=>{
    if(amount<1000){
      return "贫农"
    }else if(amount>=1000&&amount<=10000){
      return "中农"
    }else if(amount>10000){
      return "富农"
    }
  }
  return (
    <div>
      <h1 style={{color:'#f00',fontStyle:'italic'}}>人物列表</h1>
      <table>
        <thead>
          <tr>
            <th>编号</th>
            <th>姓名</th>
            <th>职位</th>
            <th>财富</th>
            <th>财富等级</th>
            <th>性别</th>
          </tr>
        </thead>
        <tbody>
          {
            persons.map((item,index)=><tr key={item.id}>
              <td>{item.id}</td>
              <td>{item.name}</td>
              <td className='job'>{item.job}</td>
              <td>{item.amount}</td> 
              <td>{getLevel(item.amount)}</td>
              <td className={item.gender?'malecolor':'fmalecolor'}>{item.gender?'男':'女'}</td>
            </tr>)
          }
        </tbody>
      </table>
    </div>
  )
}
