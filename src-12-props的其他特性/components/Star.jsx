import React, { Component } from 'react'
import './star.css'
import PropTypes from 'prop-types'
/* 
  props设置默认值的方式有两种
  1.类名.defaultProps={}
  2.在类的内部使用static关键字来进行设置
  props验证器的使用步骤
  1、下载第三方依赖包: yarn add prop-types
  2、导入PorpTypes:import PropTypes from 'prop-types'
  3.进行设置，设置的方式有两种
   1) 类名.propTypes={
         属性名：具体的验证要求
      }
   2) static propTypes={
          属性名:具体的验证要求
      }
   4.props只读性
   规则：从外部组件传递到本组件的属性都是只读的，而不在本组件修改外部传递进来的props的相关属性
   规则：React规定，虽然不能更改props的属性，但是能更改外部传递进来的对象中的属性
   React中规定可以更props中对象中的属性值，但是只是数据改变，页面没有重新渲染，如果要重新渲染
   需要调用this.forceUpdate()
*/
export default class Star extends Component {
  // //建议使用这种方式来设置默认值
  // static defaultProps={
  //   headImg:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg95.699pic.com%2Fxsj%2F12%2F5q%2Fwf.jpg%21%2Ffw%2F700%2Fwatermark%2Furl%2FL3hzai93YXRlcl9kZXRhaWwyLnBuZw%2Falign%2Fsoutheast&refer=http%3A%2F%2Fimg95.699pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657335414&t=5fe1479d37e75a41ce2c5d3dbc3292a2',
  //   name:'某女星',
  //   loveNum:0
  // }
  // static propTypes={
  //   headImg:PropTypes.string.isRequired,
  //   name:PropTypes.string.isRequired,
  //   loveNum:PropTypes.number
  // }
  incrementLoveNum=()=>{
    //  this.props.obj.loveNum=this.props.obj.loveNum+1
    //  console.log("更改后的",this.props.obj);
    //  this.forceUpdate()
    this.props.obj={
      headImg:'https://img1.baidu.com/it/u=1759334853,157694390&fm=253&fmt=auto&app=120&f=JPEG?w=400&h=382',
      name:'宋慧乔',
      loveNum:0,
    }

  }
  render() {
    console.log(this.props);
    return (
      <div className='box' onClick={this.incrementLoveNum}>
         <img src={this.props.obj.headImg} alt="" />
         <div className='title'>{this.props.obj.name}</div>
         <div className="loveNum">{this.props.obj.loveNum}</div>
      </div>
    )
  }
}

// Star.defaultProps={
//   headImg:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg95.699pic.com%2Fxsj%2F12%2F5q%2Fwf.jpg%21%2Ffw%2F700%2Fwatermark%2Furl%2FL3hzai93YXRlcl9kZXRhaWwyLnBuZw%2Falign%2Fsoutheast&refer=http%3A%2F%2Fimg95.699pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657335414&t=5fe1479d37e75a41ce2c5d3dbc3292a2',
//   name:'某女星'
// }

// Star.propTypes={
//   headImg:PropTypes.string.isRequired,
//   name:PropTypes.string.isRequired,
//   loveNum:PropTypes.number
// }

