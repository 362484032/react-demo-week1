import React, { Component,createRef } from 'react'
/* 
  React操作DOM的步骤
  1.导入createRef
  2.在构造函数中实例化ref
  3.给需要指定DOM元素添加ref属性
  4.使用
*/
export default class App extends Component {
  constructor(){
    super()
    this.boxEle=createRef()
  }
  handleboxMe=()=>{
    //获取DOM元素
    this.boxEle.current.innerHTML="Giles"
    this.boxEle.current.style.width="100px"
    this.boxEle.current.style.height="100px"
    this.boxEle.current.style.backgroundColor="#f00"
    this.boxEle.current.style.borderRadius="50%"
    this.boxEle.current.style.textAlign="center"
    this.boxEle.current.style.lineHeight="100px"

  }
  render() {
    return (
      <div>
          <div className="box" ref={this.boxEle}></div>
          <button onClick={this.handleboxMe}>点我，我就出现</button>
      </div>
    )
  }
}
