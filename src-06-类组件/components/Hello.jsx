//导入React核心包
import React from 'react'
//创建类组件，自定义类并继承React.Component
/* 
1.ES6中定义类的语法
语法结构
class 类名{
}
说明：类名规范
1）字母、数字、下划线和$符号组成
2) 不能以数字开头，不能是关键字，不能中间有空格
3） 每个单词的首字母要大写,符合驼峰式命名法
2.类中的成员
语法
class 类名{
    属性名1=初始值1;
    属性名2=初始值2;
    方法名1(){}
    方法名2(){}
}
实例化
const/let 实例化对象名=new 类名()
赋值
实例化对象名.属性名=值
实例化对象名.方法()

3.React类组件的规范
1）必须继承React.Component
2) 必须要有一个render方法:用于渲染的方法
3) render方法必须要有返回值,这个返回值就是JSX,JSX用于构建页面结构的
4) 为了让复杂的页面避免分号陷阱，需要使用小括号将其括起来
5) 为了遵守JSX必须要有一个跟元素的要求，我们可以使用<></>来将JSX括起来
6) 如果该类组件要被别的组件所引用，就必须使用ES6进行导出
*/
import Berlin from '../assets/image/Berlin.jpg'
export default class Hello extends React.Component{
    render(){
        return(
            <>
                <h1>自我介绍</h1>
                <img src={Berlin} alt="" />
                <img src={require('../assets/image/Berlin.jpg')} alt="" />
            </>
        )
    }
}
