import React from 'react'
/* 
  1.回顾vue组件的使用步骤
  1）创建以vue后缀为结尾的文件，这个文件包括三个部分
    <template>:页面结构
    script：逻辑操作
    style：样式修饰的
  2) 使用import XX from '文件路径'将其导入
  3）注册
     export default{
        components:[]
     }
  4) 使用:在<template>中以标签的形式引入
  2.react类组件的创建和引入
  1)创建类组件
  2）引入类组件
    2.1) 使用import进行导入
    2.2) 使用标签式的方式进行引入
*/

import Hello from './components/Hello'
export default function App() {
  return (
    <div>
      <Hello></Hello>     
    </div>
  )
}
