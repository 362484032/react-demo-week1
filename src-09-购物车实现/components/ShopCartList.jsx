import React, { Component } from 'react'
import '../assets/css/shopCartList.css'
export default class ShopCartList extends Component {
  state={
    shopcartList:[
        {
          id: '1001',
          imgurl: 'https://img14.360buyimg.com/n1/jfs/t1/71725/24/18991/128692/62961894E6893c2f3/2ce88c66d748fdaa.jpg',
          title: 'Dior迪奥口红烈艳蓝金999',
          price: 268.00,
          amount: 1
        },
        {
          id: '1002',
          imgurl: 'https://img13.360buyimg.com/n1/jfs/t1/100241/34/29587/365618/6295fbadEc09520db/982e80e8190ce95f.jpg',
          title: '阿玛尼红管唇釉丝绒哑光口红',
          price: 310.00,
          amount: 1
        },
        {
          id: '1003',
          imgurl: 'https://img14.360buyimg.com/n1/jfs/t1/74255/11/18744/79388/62987729E067849bf/23da11386858878b.jpg',
          title: 'YSL圣罗兰小金条细管口红',
          price: 350.00,
          amount: 2
        }
      ]
  }
  changeNum=(index,n)=>{
     let {shopcartList}=this.state;
     shopcartList[index].amount+=n
     this.setState({shopcartList:shopcartList})
  }
  rmbFormat=params=>`￥${params}.00元`
  render() {
    return (
      <div>
           <table>
         <thead>
            <tr className='titlehead'>
              <td>商品图片</td>
              <td>商品名称</td>
              <td>单价(元)</td>
              <td>数量</td>
              <td>金额(元)</td>
              <td>操作</td>
            </tr>
         </thead>
         <tbody>
            {
              this.state.shopcartList.map((item,index)=><tr key={item.id}>
                <td><img src={item.imgurl} className="shopCartPic"/></td>
                <td>{item.title}</td>
                <td>{this.rmbFormat(item.price)}</td>
                <td>
                  <button onClick={()=>{this.changeNum(index,-1)}}>-</button>
                  <input type="text" value={item.amount} className="amountInput"/>
                  <button onClick={()=>{this.changeNum(index,1)}}>+</button>
                </td>
                <td>{this.rmbFormat(item.price*item.amount)}</td>
                <td>删除</td>
              </tr>)
            }
         </tbody>
       </table>
       <div>
          总价:{this.state.shopcartList.reduce((pre,cur)=>pre+cur.price*cur.amount,0)}
       </div>
      </div>
    )
  }
}
