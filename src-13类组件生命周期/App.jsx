import React, { Component } from 'react'
/* 
  1.组件的生命周期只是针对类组件而言，函数式组件没有生命周期这个概念
  2.React类组件的生命周期可以分为三个阶段
    1) 挂载阶段
    2）运行阶段
    3）卸载阶段
  3.挂载阶段
    挂载阶段提供了三个函数
    1）constructor构造函数，构造函数中主要完成，两件事
    1.1)通过给 this.state 赋值对象来初始化内部 state。
    1.2)为事件处理函数绑定实例
    2) render方法是完成页面的渲染操作
    3) componentDidMount()的主要作用
    1.1)获取DOM节点
    1.2）发送网络请求
    1.3）设置定时器等操作
*/

import Counter from './components/Counter';
export default class App extends Component {
  constructor(){
    super()
    this.state={
      students:[],
      flag:true
    }
  }
 
  // async componentDidMount(){
  //   let result=await fetch('http://47.98.128.191:3000/students/getStudents').then(response=>response.json())
  //   this.setState((state,props)=>{
  //     return{
  //       students:result.data.rows
  //     }
  //   },()=>{
  //     console.log(this.state.students);
  //   }) 
  // }
 
  render() {
    return (
      <div>
         {this.state.flag?<Counter></Counter>:''}
         <hr />
         <button onClick={()=>{this.setState({flag:false})}}>卸载Couter组件</button>
      </div>
    )
  }
}
