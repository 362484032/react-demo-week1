import React, { Component } from 'react'

export default class Counter extends Component {
    timers=null;
    constructor() {
        console.log('------constructor--------');
        super()
        this.state = {
            count: 0,
            msg:''
        }
        this.increment = this.increment.bind(this)
        console.log(this.timers);
    }
    componentDidMount(){
        console.log('------componentDidMount---');
        // this.timers=setInterval(() => {
        //     console.log('--------走起来------------');
        // }, 1000);
    }
    increment() {
        this.setState({
            count: this.state.count + 1
        })
    }
    shouldComponentUpdate(){
        console.log('------shouldComponentUpdate-----------');
        if(this.state.count<=5){
            return false;
        }else{
            return true
        }
      }
      componentWillUnmount(){
        console.log('---------componentWillUnmount---------');
       // clearInterval(this.timers)
      }
    render() {
        console.log('-------render------------');
        return (
            <div>
                <h1>{this.state.msg}</h1>
                <h1>{this.state.count}</h1>
                <button onClick={this.increment}>+</button>
            </div>
        )
    }
}
