import React from 'react'
/* 
  1.JSX使用时候的注意事项
  1).属性名采用驼峰式命名法
  2).class属性在React中用使用className属性
  3).如果元素没有子元素，使用<元素/>来进行结束
  4).使用()包裹JSX可以避免分号陷阱
  2.回顾vue中内容的渲染和属性绑定
    1) 内容渲染:插值表达式Mustache({{}})
    2）可以使用v-html或者v-text来进行内容的渲染
    3）属性绑定: v-bind指令来完成
  3.原生的微信小程序内容和属性的渲染均采用Mustache
  4.React渲染内容和属性使用{}来完成
*/
export default function App() {
  let name="Giles"
  let age=38
  let idcard="610122198404084000"
  const birthday=idcard=>idcard.substring(6,10)+"-"+idcard.substring(10,12)+"-"+idcard.substring(12,14)
  let headImage="https://woniuimage.oss-cn-hangzhou.aliyuncs.com/woniuimage/teacher/20220222/abc7a6e787be41d0b12aa41e429b71d2.png" 
  return (
    <div>
       <div>姓名:{name}</div>
       <div>是否成年:{age>=18?'是':'否'}</div>
       <div>生日:{idcard.substring(6,10)+"-"+idcard.substring(10,12)+"-"+idcard.substring(12,14)}</div>
       <div>生日:{birthday(idcard)}</div>
       <img src={headImage} alt="" />
    </div>
  )
}
