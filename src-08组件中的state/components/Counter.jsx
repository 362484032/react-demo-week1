import React, { Component } from 'react'
/*  
   一、类组件中使用state的三个步骤
   1.定义state
   类组件中定义state一共有两种方式
   1)在类组件的构造函数中进行定义state
   2)直接定义在类组件里边，不需要前面添加this关键字
   2.使用state数据
   3.修改数据
     this.setState({变量名:新值})
  二、关于this.setState({})作用
   1、修改组件中的状态数据
   2、重新渲染页面
*/
export default class Counter extends Component {
  /* 
    ES6中constructor的作用是用来初始化数据的
    1.将堆内存空间中的新开辟的空间执行this
    2.然后通过this这这个空间添加属性和方法
    ES6继承super关键字，如果在构造函数要使用的时候先要调用父类的构造函数，必须要把父类构造函数的调用放在子类构造函数的第一行，否则会报错
  */
//   constructor(){
//     super()
//     this.state={
//         count:0
//     }
//   }
  state={
    count:0
  }
  render() {
    const {count}=this.state
    return (
      <div>
          {/* <h1>计数器:{this.state.count}</h1> */}
          <h1>计数器:{count}</h1>
          <button onClick={()=>{
              this.setState({
                  count:this.state.count+1
              })
          }}>+1</button>
      </div>
    )
  }
}
